defmodule Test.Aicacia.Handler do
  defmodule TestError do
    defexception [:message]
  end

  defmodule TestHandler do
    use Aicacia.Handler

    schema "" do
      field(:name, :string)
      field(:age, :integer, default: 18)
      field(:fail, :boolean, default: false)
    end

    def changeset(%{} = attrs) do
      %TestHandler{}
      |> cast(attrs, [:name, :age, :fail])
      |> validate_required([:name])
    end

    def handle(%{} = command) do
      if command.fail do
        {:error, %TestError{message: "fail"}}
      else
        {:ok, %{name: command.name, age: command.age}}
      end
    end
  end
end

ExUnit.start()
