defmodule Test.Aicacia.Handler.HandlerTest do
  use ExUnit.Case
  doctest Aicacia.Handler

  alias Test.Aicacia.Handler.{TestHandler, TestError}

  test "defaults" do
    assert %{name: "Bob", age: 18} = TestHandler.new!(%{name: "Bob"}) |> TestHandler.handle!()
  end

  test "handler changeset fail" do
    assert_raise(Ecto.InvalidChangesetError, fn ->
      TestHandler.new!(%{})
    end)
  end

  test "handler fail" do
    command = TestHandler.new!(%{name: "Bob", fail: true})

    assert_raise(TestError, fn ->
      TestHandler.handle!(command)
    end)
  end

  test "bad attrs" do
    {:error, _changeset} = TestHandler.new(%{})
  end
end
