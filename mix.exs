defmodule Aicacia.Handler.MixProject do
  use Mix.Project

  def project,
    do: [
      app: :aicacia_handler,
      version: "0.1.2",
      elixir: "~> 1.8",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      name: "Aicacia Handler",
      source_url: "https://gitlab.com/aicacia/libs/ex-aicacia-handler"
    ]

  def application, do: []

  def description, do: "Aicacia Handler macro"

  def package,
    do: [
      name: "aicacia_handler",
      files: ~w(lib config .formatter.exs mix.exs README* LICENSE*),
      licenses: ["Apache-2.0", "MIT"],
      links: %{"GitLab" => "https://gitlab.com/aicacia/libs/ex-aicacia-handler"}
    ]

  defp deps,
    do: [
      {:phoenix_ecto, "~> 4.0"},
      {:ex_doc, "~> 0.22", only: :dev, runtime: false}
    ]
end
