defmodule Aicacia.Handler do
  @moduledoc """
  ```elixir
  defmodule TestError do
    defexception [:message]
  end

  defmodule TestHandler do
    use Aicacia.Handler

    schema "" do
      field(:name, :string)
      field(:age, :integer, default: 18)
      field(:fail, :boolean, default: false)
    end

    def changeset(%{} = attrs) do
      %TestHandler{}
      |> cast(attrs, [:name, :age, :fail])
      |> validate_required([:name])
    end

    def handle(%{} = command) do
      if command.fail do
        {:error, %TestError{message: "fail"}}
      else
        {:ok, %{name: command.name, age: command.age}}
      end
    end
  end
  ```

  ```elixir
  %{name: "Bob", age: 18} = TestHandler.new!(%{name: "Bob"}) |> TestHandler.handle!()

  command = TestHandler.new!(%{name: "Bob", fail: true})

  assert_raise(TestError, fn ->
    TestHandler.handle!(command)
  end)

  {:error, _changeset} = TestHandler.new(%{})
  ```
  """

  @doc "ecto changeset function https://hexdocs.pm/ecto/Ecto.Changeset.html"
  @callback changeset(attrs :: Map.t()) :: Ecto.Changeset.t()
  @doc "handles the command created by `new/1` or `new!/1`"
  @callback handle(command :: Map.t()) :: {:ok, term} | {:error, term}

  @doc "creates a new changeset either returning the params or the changeset with the errors"
  @callback new(attrs :: Map.t()) :: {:ok, Map.t()} | {:error, Ecto.Changeset.t()}
  @doc "creates a new changeset either returning the params or raising the changeset"
  @callback new!(attrs :: Map.t()) :: Map.t()
  @doc "runs the handle raising on error"
  @callback handle!(command :: Map.t()) :: term

  @doc "covert this schema struct to a `Map.t()`"
  @callback to_map(struct :: Map.t()) :: Map.t()

  defmacro __using__(_opts) do
    quote do
      use Ecto.Schema
      import Ecto.Changeset

      def new(%{} = attrs) do
        cs = changeset(attrs)

        if cs.valid? do
          {:ok, to_map(apply_changes(cs))}
        else
          {:error, cs}
        end
      end

      def new!(%{} = attrs) do
        case new(attrs) do
          {:ok, map} ->
            map

          {:error, changeset} ->
            error = %Ecto.InvalidChangesetError{action: :new!, changeset: changeset}
            raise error
        end
      end

      def handle!(%{} = command) do
        case handle(command) do
          {:ok, returns} ->
            returns

          {:error, error} ->
            raise error
        end
      end

      def to_map(%{} = struct) do
        association_fields = struct.__struct__.__schema__(:associations)
        waste_fields = association_fields ++ [:__meta__]

        struct
        |> Map.from_struct()
        |> Enum.filter(fn {_, v} -> v != nil end)
        |> Enum.into(%{})
        |> Map.drop(waste_fields)
      end
    end
  end
end
